// Package helpers :nodoc:
package helpers

import (
	"context"

	"gitlab.com/bimaahida/julo-wallet/internal/config"
	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"google.golang.org/grpc/metadata"
)

// DumpIncomingContext :nodoc:
func DumpIncomingContext(c context.Context) string {
	md, _ := metadata.FromIncomingContext(c)
	return Dump(md)
}

// GetTokenFromCtx get user from context
func GetTokenFromCtx(ctx context.Context) *model.TokenClaims {
	token, ok := ctx.Value(config.WalletContextKey).(model.TokenClaims)
	if !ok {
		return nil
	}
	return &token
}
