package helpers

import "errors"

var (
	// ErrNotFound :nodoc:
	ErrNotFound = errors.New("not found")
	// ErrWalletDisable :nodoc:
	ErrWalletDisable = errors.New("disabled")
	// ErrWalletAlreadyEnable :nodoc:
	ErrWalletAlreadyEnable = errors.New("already enabled")
	// ErrWalletAlreadyDisable :nodoc:
	ErrWalletAlreadyDisable = errors.New("already disabled")
	// ErrInsufficientBalance :nodoc:
	ErrInsufficientBalance = errors.New("insufficient balance")
	// ErrTokenInvalid :nodoc:
	ErrTokenInvalid = errors.New("token is invalid")
	// ErrUserIsUnauthenticated :nodoc:
	ErrUserIsUnauthenticated = errors.New("user is unauthenticated")
	// ErrRequestBodyNotBeEmpty :nodoc:
	ErrRequestBodyNotBeEmpty = errors.New("request body can't be empty")
	// ErrorMinimumTransaction :nodoc:
	ErrorMinimumTransaction = errors.New("transactions must be greater than 0")
)
