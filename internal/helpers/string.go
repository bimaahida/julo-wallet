package helpers

import "github.com/google/uuid"

// GenerateID :nodoc:
func GenerateID() string {
	return uuid.New().String()
}
