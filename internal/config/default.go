package config

import "time"

const (
	// JetSecretKey :nodoc:
	JetSecretKey string = "my_secret_key"
	// WalletContextKey :nodoc:
	WalletContextKey string = "wallet-context"
	// DefaultExpiredJWT :nodoc:
	DefaultExpiredJWT time.Duration = 1 * time.Hour
)
