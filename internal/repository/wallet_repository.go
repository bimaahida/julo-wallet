package repository

import (
	"context"

	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"github.com/sirupsen/logrus"

	"gorm.io/gorm"
)

type walletRepo struct {
	db *gorm.DB
}

// NewWalletRepository constructor
func NewWalletRepository(
	db *gorm.DB,
) model.WalletRepository {
	return &walletRepo{
		db: db,
	}
}

// FindByID :nodoc:
func (w walletRepo) FindByID(ctx context.Context, id string) (wallet *model.Wallet, err error) {
	err = w.db.WithContext(ctx).Take(&wallet, "id = ?", id).Error
	switch err {
	case nil:
		return
	case gorm.ErrRecordNotFound:
		return nil, nil
	default:
		logrus.Error(err)
		return nil, err
	}
}

// FindByOwnedID :nodoc:
func (w walletRepo) FindByOwnedID(ctx context.Context, ownedID string) (wallet *model.Wallet, err error) {
	err = w.db.WithContext(ctx).Take(&wallet, "owned_by = ?", ownedID).Error
	switch err {
	case nil:
		return
	case gorm.ErrRecordNotFound:
		return nil, nil
	default:
		logrus.Error(err)
		return nil, err
	}
}

// Create :nodoc:
func (w walletRepo) Create(ctx context.Context, wallet *model.Wallet) (err error) {
	err = w.db.WithContext(ctx).Create(wallet).Error
	if err != nil {
		logrus.Error(err)
	}

	return
}

// Update :nodoc:
func (w walletRepo) Update(ctx context.Context, wallet *model.Wallet) (err error) {
	err = w.db.WithContext(ctx).Save(wallet).Error
	if err != nil {
		logrus.Error(err)
	}

	return
}
