package repository

import (
	"context"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"gorm.io/gorm"

	"github.com/stretchr/testify/assert"
)

func TestTransactionRepo_FindByID(t *testing.T) {
	kit, closer := initializeRepoTestKit(t)
	defer closer()

	mock := kit.mock
	repo := NewTransactionRepository(kit.db)
	ctx := context.TODO()

	const transactionID = "9b21eec8-9020-44fe-baeb-87b423aebf59"

	t.Run("should return success", func(t *testing.T) {
		mock.
			ExpectQuery(`^SELECT .+ FROM "transactions"`).
			WithArgs(transactionID).
			WillReturnRows(
				sqlmock.NewRows([]string{"id"}).
					AddRow(transactionID),
			)

		transaction, err := repo.FindByID(ctx, transactionID)
		assert.NoError(t, err)
		assert.Equal(t, transactionID, transaction.ID)
	})

	t.Run("should return not found", func(t *testing.T) {
		mock.
			ExpectQuery(`^SELECT .+ FROM "transactions"`).
			WithArgs(transactionID).
			WillReturnError(gorm.ErrRecordNotFound)

		transaction, err := repo.FindByID(ctx, transactionID)
		assert.Nil(t, err)
		assert.Nil(t, transaction)
	})

	t.Run("should return error", func(t *testing.T) {
		mock.
			ExpectQuery(`^SELECT .+ FROM "transactions"`).
			WithArgs(transactionID).
			WillReturnError(gorm.ErrInvalidDB)

		transaction, err := repo.FindByID(ctx, transactionID)
		assert.Error(t, err)
		assert.Nil(t, transaction)
	})
}

func TestTransactionRepo_Create(t *testing.T) {
	kit, closer := initializeRepoTestKit(t)
	defer closer()

	mock := kit.mock
	repo := NewTransactionRepository(kit.db)
	ctx := context.TODO()

	const (
		transactionID = "9b21eec8-9020-44fe-baeb-87b423aebf59"
		walletID      = "9b21eec8-9020-44fe-baeb-87b423aebf59"
		userID        = "74bc0cf1-51f0-452c-bdbb-d9eaceeb4747"
		referID       = "9b21eec8-9020-44fe-baeb-87b423aebf59"
	)

	transaction := model.Transaction{
		ID:          transactionID,
		Type:        model.TransactionTypeDeposit,
		Status:      model.TransactionStatusSuccess,
		Amount:      2000,
		WalletID:    walletID,
		CreatedBy:   userID,
		ReferenceID: referID,
	}

	t.Run("should create return success", func(t *testing.T) {
		mock.ExpectBegin()
		mock.
			ExpectQuery(`^INSERT INTO "transactions"`).
			WillReturnRows(
				sqlmock.NewRows([]string{"id"}).
					AddRow(113),
			)
		mock.ExpectCommit()

		err := repo.Create(ctx, &transaction)
		assert.NoError(t, err)
	})

	t.Run("should create return error", func(t *testing.T) {
		mock.ExpectBegin()
		mock.
			ExpectQuery(`^INSERT INTO "transactions"`).
			WillReturnError(gorm.ErrInvalidDB)
		mock.ExpectRollback()

		err := repo.Create(ctx, &transaction)
		assert.Error(t, err)
	})
}
