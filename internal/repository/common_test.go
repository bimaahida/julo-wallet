package repository

import (
	"log"
	"testing"

	"gitlab.com/bimaahida/julo-wallet/internal/config"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/golang/mock/gomock"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type repoTestKit struct {
	mock sqlmock.Sqlmock
	db   *gorm.DB
	ctrl *gomock.Controller
}

func initializeTest() {
	config.GetConf()
}

func initializeRepoTestKit(t *testing.T) (kit *repoTestKit, close func()) {
	initializeTest()

	dbconsole, mock, err := sqlmock.New()
	if err != nil {
		log.Fatal(err)
	}
	gormDB, err := gorm.Open(postgres.New(postgres.Config{Conn: dbconsole}), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	ctrl := gomock.NewController(t)

	tk := &repoTestKit{
		ctrl: ctrl,
		mock: mock,
		db:   gormDB,
	}

	close = func() {
		if conn, _ := tk.db.DB(); conn != nil {
			_ = conn.Close()
		}
	}

	return tk, close
}
