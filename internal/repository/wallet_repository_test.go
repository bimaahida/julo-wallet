package repository

import (
	"context"
	"testing"

	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"gorm.io/gorm"

	"github.com/DATA-DOG/go-sqlmock"

	"github.com/stretchr/testify/assert"
)

func TestWalletRepo_FindByID(t *testing.T) {
	kit, closer := initializeRepoTestKit(t)
	defer closer()

	mock := kit.mock
	repo := NewWalletRepository(kit.db)
	ctx := context.TODO()

	const (
		walletID = "9b21eec8-9020-44fe-baeb-87b423aebf59"
	)

	t.Run("should return success", func(t *testing.T) {
		mock.
			ExpectQuery(`^SELECT .+ FROM "wallets"`).
			WithArgs(walletID).
			WillReturnRows(
				sqlmock.NewRows([]string{"id"}).
					AddRow(walletID),
			)

		wallet, err := repo.FindByID(ctx, walletID)
		assert.NoError(t, err)
		assert.Equal(t, walletID, wallet.ID)
	})

	t.Run("should return not found", func(t *testing.T) {
		mock.
			ExpectQuery(`^SELECT .+ FROM "wallets"`).
			WithArgs(walletID).
			WillReturnError(gorm.ErrRecordNotFound)

		transaction, err := repo.FindByID(ctx, walletID)
		assert.Nil(t, err)
		assert.Nil(t, transaction)
	})

	t.Run("should return error", func(t *testing.T) {
		mock.
			ExpectQuery(`^SELECT .+ FROM "wallets"`).
			WithArgs(walletID).
			WillReturnError(gorm.ErrInvalidDB)

		transaction, err := repo.FindByID(ctx, walletID)
		assert.Error(t, err)
		assert.Nil(t, transaction)
	})
}

func TestWalletRepo_FindByOwnedID(t *testing.T) {
	kit, closer := initializeRepoTestKit(t)
	defer closer()

	mock := kit.mock
	repo := NewWalletRepository(kit.db)
	ctx := context.TODO()

	const (
		ownedBy  = "74bc0cf1-51f0-452c-bdbb-d9eaceeb4747"
		walletID = "9b21eec8-9020-44fe-baeb-87b423aebf59"
	)

	t.Run("should return success", func(t *testing.T) {
		mock.
			ExpectQuery(`^SELECT .+ FROM "wallets"`).
			WithArgs(ownedBy).
			WillReturnRows(
				sqlmock.NewRows([]string{"id"}).
					AddRow(walletID),
			)

		wallet, err := repo.FindByOwnedID(ctx, ownedBy)
		assert.NoError(t, err)
		assert.Equal(t, walletID, wallet.ID)
	})

	t.Run("should return not found", func(t *testing.T) {
		mock.
			ExpectQuery(`^SELECT .+ FROM "wallets"`).
			WithArgs(ownedBy).
			WillReturnError(gorm.ErrRecordNotFound)

		transaction, err := repo.FindByOwnedID(ctx, ownedBy)
		assert.Nil(t, err)
		assert.Nil(t, transaction)
	})

	t.Run("should return error", func(t *testing.T) {
		mock.
			ExpectQuery(`^SELECT .+ FROM "wallets"`).
			WithArgs(ownedBy).
			WillReturnError(gorm.ErrInvalidDB)

		transaction, err := repo.FindByOwnedID(ctx, ownedBy)
		assert.Error(t, err)
		assert.Nil(t, transaction)
	})
}

func TestWalletRepo_Create(t *testing.T) {
	kit, closer := initializeRepoTestKit(t)
	defer closer()

	mock := kit.mock
	repo := NewWalletRepository(kit.db)
	ctx := context.TODO()

	const (
		ownedBy  = "74bc0cf1-51f0-452c-bdbb-d9eaceeb4747"
		walletID = "9b21eec8-9020-44fe-baeb-87b423aebf59"
	)

	wallet := model.Wallet{
		ID:      walletID,
		OwnedBy: ownedBy,
		Balance: 0,
		Status:  model.WalletStatusDisable,
	}

	t.Run("should create return success", func(t *testing.T) {
		mock.ExpectBegin()
		mock.
			ExpectQuery(`^INSERT INTO "wallets"`).
			WillReturnRows(
				sqlmock.NewRows([]string{"id"}).
					AddRow(113),
			)
		mock.ExpectCommit()

		err := repo.Create(ctx, &wallet)
		assert.NoError(t, err)
	})

	t.Run("should create return error", func(t *testing.T) {
		mock.ExpectBegin()
		mock.
			ExpectQuery(`^INSERT INTO "wallets"`).
			WillReturnError(gorm.ErrInvalidDB)
		mock.ExpectRollback()

		err := repo.Create(ctx, &wallet)
		assert.Error(t, err)
	})
}

func TestWalletRepo_Update(t *testing.T) {
	kit, closer := initializeRepoTestKit(t)
	defer closer()

	mock := kit.mock
	repo := NewWalletRepository(kit.db)
	ctx := context.TODO()

	const (
		ownedBy  = "74bc0cf1-51f0-452c-bdbb-d9eaceeb4747"
		walletID = "9b21eec8-9020-44fe-baeb-87b423aebf59"
	)

	wallet := model.Wallet{
		ID:      walletID,
		OwnedBy: ownedBy,
		Balance: 0,
		Status:  model.WalletStatusDisable,
	}

	t.Run("should update return success", func(t *testing.T) {
		mock.ExpectBegin()
		mock.
			ExpectExec(`^UPDATE "wallets"`).
			WithArgs(wallet.OwnedBy, wallet.Balance, wallet.Status, wallet.EnabledAt, wallet.DisabledAt, wallet.CreatedAt, wallet.ID).
			WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectCommit()

		err := repo.Update(ctx, &wallet)
		assert.NoError(t, err)
	})

	t.Run("should update return error", func(t *testing.T) {
		mock.ExpectBegin()
		mock.
			ExpectExec(`^UPDATE "wallets"`).
			WithArgs(wallet.OwnedBy, wallet.Balance, wallet.Status, wallet.EnabledAt, wallet.DisabledAt, wallet.CreatedAt, wallet.ID).
			WillReturnError(gorm.ErrInvalidDB)
		mock.ExpectRollback()

		err := repo.Update(ctx, &wallet)
		assert.Error(t, err)
	})
}
