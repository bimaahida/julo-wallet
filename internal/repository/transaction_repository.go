// Package repository :nodoc:
package repository

import (
	"context"

	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"github.com/sirupsen/logrus"

	"gorm.io/gorm"
)

type transactionRepo struct {
	db *gorm.DB
}

// NewTransactionRepository constructor
func NewTransactionRepository(
	db *gorm.DB,
) model.TransactionRepository {
	return &transactionRepo{
		db: db,
	}
}

// FindByID :nodoc:
func (t transactionRepo) FindByID(ctx context.Context, id string) (transaction *model.Transaction, err error) {
	err = t.db.WithContext(ctx).Take(&transaction, "id = ?", id).Error
	switch err {
	case nil:
		return
	case gorm.ErrRecordNotFound:
		return nil, nil
	default:
		logrus.Error(err)
		return nil, err
	}
}

// Create :nodoc:
func (t transactionRepo) Create(ctx context.Context, transaction *model.Transaction) (err error) {
	err = t.db.WithContext(ctx).Create(transaction).Error
	if err != nil {
		logrus.Error(err)
	}

	return
}
