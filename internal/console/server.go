package console

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"gitlab.com/bimaahida/julo-wallet/internal/config"
	"gitlab.com/bimaahida/julo-wallet/internal/db"
	"gitlab.com/bimaahida/julo-wallet/internal/delivery/httpsvc"
	"gitlab.com/bimaahida/julo-wallet/internal/repository"
	"gitlab.com/bimaahida/julo-wallet/internal/usecase"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
)

var runCmd = &cobra.Command{
	Use:   "server",
	Short: "run server",
	Long:  `This subcommand start the server`,
	Run:   run,
}

func init() {
	RootCmd.AddCommand(runCmd)
}

func run(_ *cobra.Command, _ []string) {
	db.InitializePostgresConn()
	postgresDB, err := db.PostgresDB.DB()
	if err != nil {
		log.Fatal(err)
	}
	defer wrapCloser(postgresDB.Close)

	authMiddleware := httpsvc.NewMiddleware()

	transactionRepo := repository.NewTransactionRepository(db.PostgresDB)
	walletRepo := repository.NewWalletRepository(db.PostgresDB)

	walletUsecase := usecase.NewWalletUsecase(walletRepo)
	transactionUsecase := usecase.NewTransactionUsecase(transactionRepo, walletUsecase)

	httpServer := echo.New()
	httpServer.Use(middleware.Logger())
	httpServer.Use(middleware.Recover())

	apiGroup := httpServer.Group("/api/v1")

	walletGroup := apiGroup.Group("/wallet")
	walletGroup.Use(authMiddleware.MustAuthenticateAccessToken()) // init middleware to check token is valid and assign to context

	httpsvc.InitHTTPService(apiGroup, walletGroup, walletUsecase, transactionUsecase)

	sigCh := make(chan os.Signal, 1)
	errCh := make(chan error, 1)
	quitCh := make(chan bool, 1)

	go func() {
		for {
			select {
			case <-sigCh:
				gracefulShutdown(httpServer)
				quitCh <- true
			case e := <-errCh:
				log.Error(e)
				gracefulShutdown(httpServer)
				quitCh <- true
			}
		}
	}()

	go func() {
		// Start graphQL server
		if err := httpServer.Start(fmt.Sprintf(":%s", config.HTTPPort())); err != nil && err != http.ErrServerClosed {
			errCh <- err
		}
	}()

	<-quitCh
	log.Info("exiting")

}

func gracefulShutdown(httpSvr *echo.Echo) {
	if httpSvr != nil {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		if err := httpSvr.Shutdown(ctx); err != nil {
			httpSvr.Logger.Fatal(err)
		}
	}
}

func wrapCloser(fn func() error) {
	if err := fn(); err != nil {
		log.Error(err)
	}
}
