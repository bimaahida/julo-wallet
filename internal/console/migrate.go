// Package console :nodoc:
package console

import (
	"strconv"

	"gitlab.com/bimaahida/julo-wallet/internal/config"
	"gitlab.com/bimaahida/julo-wallet/internal/db"
	"gitlab.com/bimaahida/julo-wallet/internal/helpers"

	migrate "github.com/rubenv/sql-migrate"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "migrate database",
	Long:  `This subcommand used to migrate database`,
	Run:   processMigration,
}

func init() {
	migrateCmd.PersistentFlags().Int("step", 0, "maximum migration steps")
	migrateCmd.PersistentFlags().String("direction", "up", "migration direction")
	RootCmd.AddCommand(migrateCmd)
}

func processMigration(cmd *cobra.Command, _ []string) {
	db.InitializePostgresConn()
	direction := cmd.Flag("direction").Value.String()
	stepStr := cmd.Flag("step").Value.String()
	step, err := strconv.Atoi(stepStr)
	if err != nil {
		log.WithField("stepStr", stepStr).Fatal("Failed to parse step to int: ", err)
	}

	migrations := &migrate.FileMigrationSource{
		Dir: "./db/migration",
	}

	migrate.SetTable("schema_migrations")
	database, err := db.PostgresDB.DB()
	if err != nil {
		log.WithField("DatabaseDSN", config.DatabaseDSN()).Fatal("Failed to connect database: ", err)
	}

	var n int
	if direction == "down" {
		n, err = migrate.ExecMax(database, "postgres", migrations, migrate.Down, step)
	} else {
		n, err = migrate.ExecMax(database, "postgres", migrations, migrate.Up, step)
	}
	if err != nil {
		log.WithFields(log.Fields{
			"migrations": helpers.Dump(migrations),
			"direction":  direction,
			"step":       step,
		}).Fatal("Failed to migrate database: ", err)
	}

	log.Infof("Applied %d migrations!\n", n)
}
