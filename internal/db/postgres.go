// Package db :nodoc:
package db

import (
	"gitlab.com/bimaahida/julo-wallet/internal/config"

	log "github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var (
	// PostgresDB represents gorm DB
	PostgresDB *gorm.DB
)

// InitializePostgresConn :nodoc:
func InitializePostgresConn() {
	conn, err := openConn(config.DatabaseDSN())
	if err != nil {
		log.WithField("databaseDSN", config.DatabaseDSN()).Fatal("failed to connect database: ", err)
	}

	PostgresDB = conn

	log.Info("Connection to database success...")
}

func openConn(dsn string) (*gorm.DB, error) {
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	return db, nil
}
