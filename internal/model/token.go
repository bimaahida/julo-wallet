// Package model :nodoc:
package model

import "github.com/golang-jwt/jwt/v4"

// TokenClaims :nodoc:
type TokenClaims struct {
	jwt.RegisteredClaims
	UserID   string `json:"user_id"`
	WalletID string `json:"wallet_id"`
}

// TokenResponse :nodoc:
type TokenResponse struct {
	Token string `json:"token"`
}

// TokenInput :nodoc:
type TokenInput struct {
	CustomerXID string `json:"customer_xid" form:"customer_xid" validate:"required"`
}
