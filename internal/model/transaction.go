package model

import (
	"context"
	"time"
)

const (
	// MinimumTransaction :nodoc:
	MinimumTransaction float64 = 0
)

// TransactionUsecase :nodoc:
type TransactionUsecase interface {
	Deposit(ctx context.Context, userID, walletID string, amount float64, referID string) (*DepositResponse, error)
	Withdraw(ctx context.Context, userID, walletID string, amount float64, referID string) (*WithdrawResponse, error)
}

// TransactionRepository :nodoc:
type TransactionRepository interface {
	FindByID(ctx context.Context, ID string) (*Transaction, error)
	Create(ctx context.Context, transaction *Transaction) error
}

// WithdrawResponse :nodoc:
type WithdrawResponse struct {
	ID          string            `json:"id"`
	Status      TransactionStatus `json:"status"`
	WithdrawnBy string            `json:"withdrawn_by"`
	WithdrawnAt time.Time         `json:"withdrawn_at"`
	Amount      float64           `json:"amount"`
	ReferenceID string            `json:"reference_id"`
}

// DepositResponse :nodoc:
type DepositResponse struct {
	ID          string            `json:"id"`
	Status      TransactionStatus `json:"status"`
	DepositBy   string            `json:"deposit_by"`
	DepositAt   time.Time         `json:"deposit_at"`
	Amount      float64           `json:"amount"`
	ReferenceID string            `json:"reference_id"`
}

// TransactionType :nodoc:
type TransactionType string

// TransactionType constants :nodoc:
const (
	TransactionTypeWithdraw TransactionType = "withdraw"
	TransactionTypeDeposit  TransactionType = "deposit"
)

// TransactionStatus :nodoc:
type TransactionStatus string

// TransactionStatus constants :nodoc:
const (
	TransactionStatusSuccess TransactionStatus = "success"
	TransactionStatusFailed  TransactionStatus = "failed"
)

// Transaction :nodoc:
type Transaction struct {
	ID          string            `gorm:"primary_key" json:"id"`
	Type        TransactionType   `json:"type"`
	Status      TransactionStatus `json:"status"`
	Amount      float64           `json:"amount"`
	WalletID    string            `json:"wallet_id"`
	CreatedBy   string            `json:"created_by"`
	ReferenceID string            `json:"reference_id"`
	CreatedAt   time.Time         `json:"created_at,omitempty"`
}

// ToDepositType :nodoc:
func (t *Transaction) ToDepositType() *DepositResponse {
	return &DepositResponse{
		ID:          t.ID,
		Status:      t.Status,
		DepositBy:   t.CreatedBy,
		DepositAt:   t.CreatedAt,
		Amount:      t.Amount,
		ReferenceID: t.ReferenceID,
	}
}

// ToWithdrawType :nodoc:
func (t *Transaction) ToWithdrawType() *WithdrawResponse {
	return &WithdrawResponse{
		ID:          t.ID,
		Status:      t.Status,
		WithdrawnBy: t.CreatedBy,
		WithdrawnAt: t.CreatedAt,
		Amount:      t.Amount,
		ReferenceID: t.ReferenceID,
	}
}

// InputTransaction :nodoc:
type InputTransaction struct {
	Amount      float64 `json:"amount" form:"amount" validate:"required,gt=0"`
	ReferenceID string  `json:"reference_id" form:"reference_id" validate:"required"`
}
