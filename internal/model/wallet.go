package model

import (
	"context"
	"time"

	"gopkg.in/guregu/null.v4"
)

// WalletUsecase :nodoc:
type WalletUsecase interface {
	FindByID(ctx context.Context, walletID string) (*Wallet, error)
	Create(ctx context.Context, userID string) (*Wallet, error)
	UpdateBalance(ctx context.Context, transactionType TransactionType, walletID string, amount float64) (*Wallet, error)
	UpdateStatus(ctx context.Context, walletID string, status WalletStatus) (*Wallet, error)
}

// WalletRepository :nodoc:
type WalletRepository interface {
	FindByID(ctx context.Context, id string) (*Wallet, error)
	FindByOwnedID(ctx context.Context, ownedID string) (*Wallet, error)
	Create(ctx context.Context, wallet *Wallet) error
	Update(ctx context.Context, wallet *Wallet) error
}

// WalletStatus :nodoc:
type WalletStatus string

// WalletStatus constants :nodoc:
const (
	WalletStatusEnable  WalletStatus = "enable"
	WalletStatusDisable WalletStatus = "disable"
)

// Wallet :nodoc:
type Wallet struct {
	ID         string       `gorm:"primary_key" json:"id"`
	OwnedBy    string       `json:"owned_by"`
	Balance    float64      `json:"balance"`
	Status     WalletStatus `json:"status"`
	EnabledAt  null.Time    `json:"enabled_at,omitempty"`
	DisabledAt null.Time    `json:"disabled_at,omitempty"`
	CreatedAt  time.Time    `json:"created_at,omitempty"`
}

// ToResponse :nodoc:
func (w *Wallet) ToResponse() WalletResponse {
	return WalletResponse{
		ID:        w.ID,
		OwnedBy:   w.OwnedBy,
		Balance:   w.Balance,
		Status:    w.Status,
		EnabledAt: w.EnabledAt,
	}
}

// WalletResponse :nodoc:
type WalletResponse struct {
	ID        string       `gorm:"primary_key" json:"id"`
	OwnedBy   string       `json:"owned_by"`
	Balance   float64      `json:"balance"`
	Status    WalletStatus `json:"status"`
	EnabledAt null.Time    `json:"enabled_at,omitempty"`
}
