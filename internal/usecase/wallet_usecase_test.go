package usecase

import (
	"context"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/bimaahida/julo-wallet/internal/model"
	"gitlab.com/bimaahida/julo-wallet/internal/model/mock"
	"gopkg.in/guregu/null.v4"
	"gorm.io/gorm"
)

func TestWalletUsecase_FindByID(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockWalletRepo := mock.NewMockWalletRepository(ctrl)
	usecase := NewWalletUsecase(mockWalletRepo)

	var (
		ctx      = context.TODO()
		userID   = "74bc0cf1-51f0-452c-bdbb-d9eaceeb4747"
		walletID = "9b21eec8-9020-44fe-baeb-87b423aebf59"
		amount   = float64(1000)
	)

	t.Run("should success find by id", func(t *testing.T) {
		newWallet := model.Wallet{
			ID:        walletID,
			OwnedBy:   userID,
			Balance:   amount,
			Status:    model.WalletStatusEnable,
			EnabledAt: null.NewTime(time.Now(), true),
		}

		mockWalletRepo.
			EXPECT().
			FindByID(ctx, walletID).
			Times(1).
			Return(&newWallet, nil)

		wallet, err := usecase.FindByID(ctx, walletID)
		assert.NoError(t, err)
		assert.Equal(t, newWallet.ID, wallet.ID)
		assert.Equal(t, newWallet.Status, wallet.Status)
		assert.Equal(t, newWallet.Balance, wallet.Balance)
	})

	t.Run("should error find by id", func(t *testing.T) {
		mockWalletRepo.
			EXPECT().
			FindByID(ctx, walletID).
			Times(1).
			Return(nil, gorm.ErrInvalidDB)

		wallet, err := usecase.FindByID(ctx, walletID)
		assert.Error(t, err)
		assert.Nil(t, wallet)
	})

}

func TestWalletUsecase_Create(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockWalletRepo := mock.NewMockWalletRepository(ctrl)
	usecase := NewWalletUsecase(mockWalletRepo)

	var (
		ctx      = context.TODO()
		userID   = "74bc0cf1-51f0-452c-bdbb-d9eaceeb4747"
		walletID = "9b21eec8-9020-44fe-baeb-87b423aebf59"
		amount   = float64(1000)
	)

	t.Run("should success get wallet without create", func(t *testing.T) {
		newWallet := model.Wallet{
			ID:        walletID,
			OwnedBy:   userID,
			Balance:   amount,
			Status:    model.WalletStatusEnable,
			EnabledAt: null.NewTime(time.Now(), true),
		}

		mockWalletRepo.
			EXPECT().
			FindByOwnedID(ctx, userID).
			Times(1).
			Return(&newWallet, nil)

		wallet, err := usecase.Create(ctx, userID)
		assert.NoError(t, err)
		assert.Equal(t, newWallet.ID, wallet.ID)
		assert.Equal(t, newWallet.Status, wallet.Status)
		assert.Equal(t, newWallet.Balance, wallet.Balance)
	})

	t.Run("should success get wallet with success create", func(t *testing.T) {
		newWallet := model.Wallet{
			ID:      walletID,
			OwnedBy: userID,
			Balance: float64(0),
			Status:  model.WalletStatusDisable,
		}

		mockWalletRepo.
			EXPECT().
			FindByOwnedID(ctx, userID).
			Times(1).
			Return(nil, nil)

		mockWalletRepo.
			EXPECT().
			Create(ctx, gomock.Any()).
			Times(1).
			Return(nil)

		mockWalletRepo.
			EXPECT().
			FindByID(ctx, gomock.All()).
			Times(1).
			Return(&newWallet, nil)

		wallet, err := usecase.Create(ctx, userID)
		assert.NoError(t, err)
		assert.Equal(t, model.WalletStatusDisable, wallet.Status)
		assert.Equal(t, newWallet.Balance, wallet.Balance)
	})

	t.Run("should error get wallet", func(t *testing.T) {
		mockWalletRepo.
			EXPECT().
			FindByOwnedID(ctx, userID).
			Times(1).
			Return(nil, gorm.ErrInvalidDB)

		wallet, err := usecase.Create(ctx, userID)
		assert.Error(t, err)
		assert.Nil(t, wallet)
	})

	t.Run("should success get wallet with error create", func(t *testing.T) {
		mockWalletRepo.
			EXPECT().
			FindByOwnedID(ctx, userID).
			Times(1).
			Return(nil, nil)

		mockWalletRepo.
			EXPECT().
			Create(ctx, gomock.Any()).
			Times(1).
			Return(gorm.ErrInvalidDB)

		wallet, err := usecase.Create(ctx, userID)
		assert.Error(t, err)
		assert.Nil(t, wallet)
	})

	t.Run("should success get wallet with success create and error find", func(t *testing.T) {
		mockWalletRepo.
			EXPECT().
			FindByOwnedID(ctx, userID).
			Times(1).
			Return(nil, nil)

		mockWalletRepo.
			EXPECT().
			Create(ctx, gomock.Any()).
			Times(1).
			Return(nil)

		mockWalletRepo.
			EXPECT().
			FindByID(ctx, gomock.All()).
			Times(1).
			Return(nil, gorm.ErrInvalidDB)

		wallet, err := usecase.Create(ctx, userID)
		assert.Error(t, err)
		assert.Nil(t, wallet)
	})
}

func TestWalletUsecase_UpdateBalance(t *testing.T) {

}

func TestWalletUsecase_UpdateStatus(t *testing.T) {

}
