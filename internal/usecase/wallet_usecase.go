package usecase

import (
	"context"
	"time"

	"gitlab.com/bimaahida/julo-wallet/internal/helpers"
	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"github.com/sirupsen/logrus"
	"gopkg.in/guregu/null.v4"
)

// walletUsecase :nodoc:
type walletUsecase struct {
	walletRepo model.WalletRepository
}

// NewWalletUsecase constructs WalletUsecase
func NewWalletUsecase(walletRepo model.WalletRepository) model.WalletUsecase {
	return &walletUsecase{
		walletRepo: walletRepo,
	}
}

// FindByID :nodoc:
func (w walletUsecase) FindByID(ctx context.Context, walletID string) (*model.Wallet, error) {
	return w.walletRepo.FindByID(ctx, walletID)
}

// Create :nodoc:
func (w walletUsecase) Create(ctx context.Context, userID string) (*model.Wallet, error) {
	logger := logrus.WithFields(logrus.Fields{
		"ctx":    helpers.DumpIncomingContext(ctx),
		"userID": userID,
	})
	existingWallet, err := w.walletRepo.FindByOwnedID(ctx, userID)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	if existingWallet != nil {
		return existingWallet, nil
	}

	newWallet := &model.Wallet{
		ID:      helpers.GenerateID(),
		OwnedBy: userID,
		Balance: 0,
		Status:  model.WalletStatusDisable,
	}

	err = w.walletRepo.Create(ctx, newWallet)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return w.FindByID(ctx, newWallet.ID)
}

// UpdateBalance :nodoc:
func (w walletUsecase) UpdateBalance(ctx context.Context, transactionType model.TransactionType, walletID string, amount float64) (*model.Wallet, error) {
	logger := logrus.WithFields(logrus.Fields{
		"ctx":             helpers.DumpIncomingContext(ctx),
		"transactionType": transactionType,
		"walletID":        walletID,
		"amount":          amount,
	})

	existingWallet, err := w.walletRepo.FindByID(ctx, walletID)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	if existingWallet == nil {
		return nil, helpers.ErrNotFound
	}

	if existingWallet.Status != model.WalletStatusEnable {
		return nil, helpers.ErrWalletDisable
	}

	if err = balanceCalculation(transactionType, amount, existingWallet); err != nil {
		return nil, err
	}

	err = w.walletRepo.Update(ctx, existingWallet)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return w.FindByID(ctx, walletID)
}

// UpdateStatus :nodoc:
func (w walletUsecase) UpdateStatus(ctx context.Context, walletID string, status model.WalletStatus) (*model.Wallet, error) {
	logger := logrus.WithFields(logrus.Fields{
		"ctx":      helpers.DumpIncomingContext(ctx),
		"walletID": walletID,
		"status":   status,
	})

	existingWallet, err := w.walletRepo.FindByID(ctx, walletID)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	if existingWallet == nil {
		return nil, helpers.ErrNotFound
	}

	if err = validateStatus(existingWallet.Status, status); err != nil {
		return nil, err
	}

	existingWallet.Status = status
	switch status {
	case model.WalletStatusDisable:
		existingWallet.DisabledAt = null.NewTime(time.Now(), true)
	case model.WalletStatusEnable:
		existingWallet.EnabledAt = null.NewTime(time.Now(), true)
	}

	err = w.walletRepo.Update(ctx, existingWallet)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return w.FindByID(ctx, walletID)
}

func validateStatus(existingStatus, newStatus model.WalletStatus) error {
	if existingStatus != newStatus {
		return nil
	}

	if newStatus == model.WalletStatusEnable {
		return helpers.ErrWalletAlreadyEnable
	}

	if newStatus == model.WalletStatusDisable {
		return helpers.ErrWalletAlreadyDisable
	}

	return nil
}

func balanceCalculation(transactionType model.TransactionType, amount float64, existingWallet *model.Wallet) error {
	if transactionType == model.TransactionTypeDeposit {
		existingWallet.Balance += amount
		return nil
	}

	if existingWallet.Balance >= amount {
		existingWallet.Balance -= amount
		return nil
	}

	return helpers.ErrInsufficientBalance
}
