package usecase

import (
	"context"
	"testing"

	"gorm.io/gorm"

	"github.com/stretchr/testify/assert"
	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"gitlab.com/bimaahida/julo-wallet/internal/model/mock"

	"github.com/golang/mock/gomock"
)

func TestTransactionUsecase_Deposit(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockTransactionRepo := mock.NewMockTransactionRepository(ctrl)
	mockWalletUsecase := mock.NewMockWalletUsecase(ctrl)
	usecase := NewTransactionUsecase(mockTransactionRepo, mockWalletUsecase)

	var (
		ctx      = context.TODO()
		userID   = "74bc0cf1-51f0-452c-bdbb-d9eaceeb4747"
		walletID = "9b21eec8-9020-44fe-baeb-87b423aebf59"
		amount   = float64(1000)
		referID  = "9b21eec8-9020-44fe-baeb-87b423aebf59"
	)

	t.Run("should success deposit", func(t *testing.T) {
		mockWalletUsecase.
			EXPECT().
			UpdateBalance(ctx, model.TransactionTypeDeposit, walletID, amount).
			Times(1).
			Return(&model.Wallet{}, nil)
		mockTransactionRepo.
			EXPECT().
			Create(ctx, gomock.Any()).
			Times(1).
			Return(nil)

		deposit, err := usecase.Deposit(ctx, userID, walletID, amount, referID)
		assert.NoError(t, err)
		assert.Equal(t, model.TransactionStatusSuccess, deposit.Status)
		assert.Equal(t, userID, deposit.DepositBy)
		assert.Equal(t, amount, deposit.Amount)
	})

	t.Run("should error deposit", func(t *testing.T) {
		mockWalletUsecase.
			EXPECT().
			UpdateBalance(ctx, model.TransactionTypeDeposit, walletID, amount).
			Times(1).
			Return(&model.Wallet{}, gorm.ErrInvalidDB)
		mockTransactionRepo.
			EXPECT().
			Create(ctx, gomock.Any()).
			Times(1).
			Return(nil)

		deposit, err := usecase.Deposit(ctx, userID, walletID, amount, referID)
		assert.NoError(t, err)
		assert.Equal(t, model.TransactionStatusFailed, deposit.Status)
		assert.Equal(t, userID, deposit.DepositBy)
		assert.Equal(t, amount, deposit.Amount)
	})
}

func TestTransactionUsecase_Withdraw(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockTransactionRepo := mock.NewMockTransactionRepository(ctrl)
	mockWalletUsecase := mock.NewMockWalletUsecase(ctrl)
	usecase := NewTransactionUsecase(mockTransactionRepo, mockWalletUsecase)

	var (
		ctx      = context.TODO()
		userID   = "74bc0cf1-51f0-452c-bdbb-d9eaceeb4747"
		walletID = "9b21eec8-9020-44fe-baeb-87b423aebf59"
		amount   = float64(1000)
		referID  = "9b21eec8-9020-44fe-baeb-87b423aebf59"
	)

	t.Run("should success withdraw", func(t *testing.T) {
		mockWalletUsecase.
			EXPECT().
			UpdateBalance(ctx, model.TransactionTypeWithdraw, walletID, amount).
			Times(1).
			Return(&model.Wallet{}, nil)
		mockTransactionRepo.
			EXPECT().
			Create(ctx, gomock.Any()).
			Times(1).
			Return(nil)

		deposit, err := usecase.Withdraw(ctx, userID, walletID, amount, referID)
		assert.NoError(t, err)
		assert.Equal(t, model.TransactionStatusSuccess, deposit.Status)
		assert.Equal(t, userID, deposit.WithdrawnBy)
		assert.Equal(t, amount, deposit.Amount)
	})

	t.Run("should error withdraw", func(t *testing.T) {
		mockWalletUsecase.
			EXPECT().
			UpdateBalance(ctx, model.TransactionTypeWithdraw, walletID, amount).
			Times(1).
			Return(&model.Wallet{}, gorm.ErrInvalidDB)
		mockTransactionRepo.
			EXPECT().
			Create(ctx, gomock.Any()).
			Times(1).
			Return(nil)

		deposit, err := usecase.Withdraw(ctx, userID, walletID, amount, referID)
		assert.NoError(t, err)
		assert.Equal(t, model.TransactionStatusFailed, deposit.Status)
		assert.Equal(t, userID, deposit.WithdrawnBy)
		assert.Equal(t, amount, deposit.Amount)
	})
}
