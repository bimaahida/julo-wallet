// Package usecase :nodoc:
package usecase

import (
	"context"

	"gitlab.com/bimaahida/julo-wallet/internal/helpers"
	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"github.com/sirupsen/logrus"
)

// transactionUsecase :nodoc:
type transactionUsecase struct {
	transactionRepository model.TransactionRepository
	walletUsecase         model.WalletUsecase
}

// NewTransactionUsecase constructs WalletUsecase
func NewTransactionUsecase(
	transactionRepository model.TransactionRepository,
	walletUsecase model.WalletUsecase,
) model.TransactionUsecase {
	return &transactionUsecase{
		transactionRepository: transactionRepository,
		walletUsecase:         walletUsecase,
	}
}

// Deposit :nodoc:
func (t transactionUsecase) Deposit(ctx context.Context, userID, walletID string, amount float64, referID string) (*model.DepositResponse, error) {
	logger := logrus.WithFields(logrus.Fields{
		"ctx":      helpers.DumpIncomingContext(ctx),
		"walletID": walletID,
		"amount":   amount,
	})

	newDeposit := model.Transaction{
		ID:          helpers.GenerateID(),
		Type:        model.TransactionTypeDeposit,
		Status:      model.TransactionStatusSuccess,
		Amount:      amount,
		WalletID:    walletID,
		CreatedBy:   userID,
		ReferenceID: referID,
	}

	_, err := t.walletUsecase.UpdateBalance(ctx, model.TransactionTypeDeposit, walletID, amount)
	if err != nil {
		logger.Error(err)
		newDeposit.Status = model.TransactionStatusFailed
	}

	err = t.transactionRepository.Create(ctx, &newDeposit)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return newDeposit.ToDepositType(), nil
}

// Withdraw :nodoc:
func (t transactionUsecase) Withdraw(ctx context.Context, userID, walletID string, amount float64, referID string) (*model.WithdrawResponse, error) {
	logger := logrus.WithFields(logrus.Fields{
		"ctx":      helpers.DumpIncomingContext(ctx),
		"walletID": walletID,
		"amount":   amount,
	})

	newDeposit := model.Transaction{
		ID:          helpers.GenerateID(),
		Type:        model.TransactionTypeWithdraw,
		Status:      model.TransactionStatusSuccess,
		Amount:      amount,
		WalletID:    walletID,
		CreatedBy:   userID,
		ReferenceID: referID,
	}

	_, err := t.walletUsecase.UpdateBalance(ctx, model.TransactionTypeWithdraw, walletID, amount)
	if err != nil {
		logger.Error(err)
		newDeposit.Status = model.TransactionStatusFailed
	}

	err = t.transactionRepository.Create(ctx, &newDeposit)
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	return newDeposit.ToWithdrawType(), nil
}
