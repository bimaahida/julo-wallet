package httpsvc

import (
	"net/http"

	"github.com/labstack/echo"
)

// StatusType :nodoc:
type StatusType string

// StatusType const
const (
	StatusError   StatusType = "error"
	StatusFail    StatusType = "fail"
	StatusSuccess StatusType = "success"
)

func (st StatusType) toString() string {
	return string(st)
}

// Body contains
type Body struct {
	Status string      `json:"status"`
	Data   interface{} `json:"data"`
}

// NewResponse returns a success body with the given data.
func NewResponse(responseType StatusType, data interface{}) Body {
	return Body{
		Status: responseType.toString(),
		Data:   data,
	}
}

// WrapHTTPResponse writes failed body with the given data.
func WrapHTTPResponse(c echo.Context, responseType StatusType, code int, data interface{}) error {
	if responseType == StatusSuccess {
		return c.JSON(code, NewResponse(responseType, data))
	}

	parseError, ok := data.(error)
	if !ok {
		parseError = echo.NewHTTPError(http.StatusInternalServerError)
	}

	return echo.NewHTTPError(code, NewResponse(responseType, parseError.Error()))
}
