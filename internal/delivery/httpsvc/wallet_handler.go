package httpsvc

import (
	"net/http"

	"gitlab.com/bimaahida/julo-wallet/internal/helpers"
	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
)

func (s *Service) enableWallet(c echo.Context) error {
	ctx := c.Request().Context()
	token := helpers.GetTokenFromCtx(ctx)

	logger := logrus.WithFields(
		logrus.Fields{
			"ctx":   helpers.DumpIncomingContext(ctx),
			"token": helpers.Dump(token),
		},
	)

	wallet, err := s.walletUsecase.UpdateStatus(ctx, token.WalletID, model.WalletStatusEnable)
	switch err {
	case nil:
		return WrapHTTPResponse(c, StatusSuccess, http.StatusOK, wallet.ToResponse())
	case helpers.ErrWalletAlreadyEnable:
		logger.Error(err)
		return WrapHTTPResponse(c, StatusError, http.StatusBadRequest, err)
	default:
		logger.Error(err)
		return WrapHTTPResponse(c, StatusError, http.StatusInternalServerError, err)
	}
}

func (s *Service) disableWallet(c echo.Context) error {
	ctx := c.Request().Context()
	token := helpers.GetTokenFromCtx(ctx)

	logger := logrus.WithFields(
		logrus.Fields{
			"ctx":   helpers.DumpIncomingContext(ctx),
			"token": helpers.Dump(token),
		},
	)

	wallet, err := s.walletUsecase.UpdateStatus(ctx, token.WalletID, model.WalletStatusDisable)
	switch err {
	case nil:
		return WrapHTTPResponse(c, StatusSuccess, http.StatusOK, wallet.ToResponse())
	case helpers.ErrWalletAlreadyEnable:
		logger.Error(err)
		return WrapHTTPResponse(c, StatusError, http.StatusBadRequest, err)
	default:
		logger.Error(err)
		return WrapHTTPResponse(c, StatusError, http.StatusInternalServerError, err)
	}
}

func (s *Service) getWallet(c echo.Context) error {
	ctx := c.Request().Context()
	token := helpers.GetTokenFromCtx(ctx)

	wallet, err := s.walletUsecase.FindByID(ctx, token.WalletID)
	if err != nil {
		logrus.WithFields(
			logrus.Fields{
				"ctx":   helpers.DumpIncomingContext(ctx),
				"token": helpers.Dump(token),
			},
		).Error(err)
		return WrapHTTPResponse(c, StatusError, http.StatusInternalServerError, err)
	}

	return WrapHTTPResponse(c, StatusSuccess, http.StatusOK, wallet.ToResponse())
}

func (s *Service) deposit(c echo.Context) error {
	ctx := c.Request().Context()
	token := helpers.GetTokenFromCtx(ctx)

	transaction := new(model.InputTransaction)
	err := c.Bind(transaction)
	if err != nil {
		logrus.Error(err)
		return WrapHTTPResponse(c, StatusFail, http.StatusBadRequest, helpers.ErrRequestBodyNotBeEmpty)
	}

	logger := logrus.WithFields(
		logrus.Fields{
			"ctx":         helpers.DumpIncomingContext(ctx),
			"transaction": helpers.Dump(transaction),
		},
	)

	// validation body
	if err = Validator.Struct(transaction); err != nil {
		logger.Error(err)
		return WrapHTTPResponse(c, StatusFail, http.StatusBadRequest, err)
	}

	if transaction.Amount <= model.MinimumTransaction {
		return WrapHTTPResponse(c, StatusFail, http.StatusBadRequest, helpers.ErrorMinimumTransaction)
	}

	wallet, err := s.transactionUsecase.Deposit(ctx, token.UserID, token.WalletID, transaction.Amount, transaction.ReferenceID)
	if err != nil {
		logger.Error(err)
		return WrapHTTPResponse(c, StatusError, http.StatusInternalServerError, err)
	}

	return WrapHTTPResponse(c, StatusSuccess, http.StatusOK, wallet)
}

func (s *Service) withdraw(c echo.Context) error {
	ctx := c.Request().Context()
	token := helpers.GetTokenFromCtx(ctx)

	transaction := new(model.InputTransaction)
	err := c.Bind(transaction)
	if err != nil {
		logrus.Error(err)
		return WrapHTTPResponse(c, StatusFail, http.StatusBadRequest, helpers.ErrRequestBodyNotBeEmpty)
	}

	logger := logrus.WithFields(
		logrus.Fields{
			"ctx":         helpers.DumpIncomingContext(ctx),
			"transaction": helpers.Dump(transaction),
		},
	)

	// validation body
	if err = Validator.Struct(transaction); err != nil {
		logger.Error(err)
		return WrapHTTPResponse(c, StatusFail, http.StatusBadRequest, err)
	}

	wallet, err := s.transactionUsecase.Withdraw(ctx, token.UserID, token.WalletID, transaction.Amount, transaction.ReferenceID)
	if err != nil {
		logger.Error(err)
		return WrapHTTPResponse(c, StatusError, http.StatusInternalServerError, err)
	}

	return WrapHTTPResponse(c, StatusSuccess, http.StatusOK, wallet)
}
