package httpsvc

import (
	"net/http"
	"time"

	"gitlab.com/bimaahida/julo-wallet/internal/helpers"
	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"github.com/sirupsen/logrus"

	"gitlab.com/bimaahida/julo-wallet/internal/config"

	"github.com/golang-jwt/jwt/v4"
	"github.com/labstack/echo"
)

func (s *Service) initTokenWallet(c echo.Context) error {
	ctx := c.Request().Context()

	tokenInput := new(model.TokenInput)
	err := c.Bind(tokenInput)
	if err != nil {
		logrus.Error(err)
		return WrapHTTPResponse(c, StatusFail, http.StatusBadRequest, helpers.ErrRequestBodyNotBeEmpty)
	}

	logger := logrus.WithFields(
		logrus.Fields{
			"ctx":        helpers.DumpIncomingContext(ctx),
			"tokenInput": helpers.Dump(tokenInput),
		},
	)

	// validation body
	if err = Validator.Struct(tokenInput); err != nil {
		logger.Error(err)
		return WrapHTTPResponse(c, StatusFail, http.StatusBadRequest, err)
	}

	wallet, err := s.walletUsecase.Create(ctx, tokenInput.CustomerXID)
	if err != nil {
		logger.Error(err)
		return WrapHTTPResponse(c, StatusError, http.StatusInternalServerError, err)
	}

	expirationTime := time.Now().Add(config.DefaultExpiredJWT)

	// Create the JWT claims, which includes the UserID and expiry time
	token := model.TokenClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
		},
		UserID:   tokenInput.CustomerXID,
		WalletID: wallet.ID,
	}

	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, token)
	tokenString, err := jwtToken.SignedString([]byte(config.JetSecretKey))
	if err != nil {
		logger.Error(err)
		return WrapHTTPResponse(c, StatusError, http.StatusInternalServerError, err)
	}

	return WrapHTTPResponse(c, StatusSuccess, http.StatusOK, model.TokenResponse{
		Token: tokenString,
	})
}
