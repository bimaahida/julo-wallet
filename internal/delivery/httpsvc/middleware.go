// Package httpsvc :nodoc:
package httpsvc

import (
	"context"
	"net/http"
	"strings"

	"gitlab.com/bimaahida/julo-wallet/internal/config"
	"gitlab.com/bimaahida/julo-wallet/internal/helpers"
	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"github.com/golang-jwt/jwt/v4"
	"github.com/labstack/echo"
)

const (
	_authScheme          = "Token"
	_headerAuthorization = "Authorization"
)

// Middleware :nodoc:
type Middleware struct {
}

// NewMiddleware constructor
func NewMiddleware() *Middleware {
	return &Middleware{}
}

// MustAuthenticateAccessToken must authenticate access token from http `Authorization` header and load a User to context
func (m *Middleware) MustAuthenticateAccessToken() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			headerToken := getAccessToken(c.Request())
			if headerToken == "" {
				return WrapHTTPResponse(c, StatusFail, http.StatusUnauthorized, helpers.ErrUserIsUnauthenticated)
			}

			tokenClaims := model.TokenClaims{}
			tkn, err := jwt.ParseWithClaims(headerToken, &tokenClaims, func(token *jwt.Token) (interface{}, error) {
				return []byte(config.JetSecretKey), nil
			})
			if err != nil || !tkn.Valid {
				return WrapHTTPResponse(c, StatusFail, http.StatusUnauthorized, helpers.ErrTokenInvalid)
			}

			ctx := context.WithValue(c.Request().Context(), config.WalletContextKey, tokenClaims) //nolint:all
			c.SetRequest(c.Request().WithContext(ctx))
			return next(c)
		}
	}
}

func getAccessToken(req *http.Request) (accessToken string) {
	authHeaders := strings.Split(req.Header.Get(_headerAuthorization), " ")

	if (len(authHeaders) != 2) || (authHeaders[0] != _authScheme) {
		return ""
	}

	return strings.TrimSpace(authHeaders[1])
}
