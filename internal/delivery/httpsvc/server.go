package httpsvc

import (
	"gitlab.com/bimaahida/julo-wallet/internal/model"

	"gopkg.in/go-playground/validator.v9"

	"github.com/labstack/echo"
)

// Validator use for validator struct
var Validator *validator.Validate

// Service http service
type Service struct {
	defaultGroup       *echo.Group
	walletGroup        *echo.Group
	walletUsecase      model.WalletUsecase
	transactionUsecase model.TransactionUsecase
}

// InitHTTPService API route constructor
func InitHTTPService(
	defaultGroup *echo.Group,
	walletGroup *echo.Group,
	walletUsecase model.WalletUsecase,
	transactionUsecase model.TransactionUsecase,
) {
	// initiate validator
	Validator = validator.New()

	srv := &Service{
		defaultGroup:       defaultGroup,
		walletGroup:        walletGroup,
		walletUsecase:      walletUsecase,
		transactionUsecase: transactionUsecase,
	}
	srv.initRoutes()
}

func (s *Service) initRoutes() {
	// token Group
	s.defaultGroup.POST("/init", s.initTokenWallet)

	// Wallet Group
	s.walletGroup.POST("", s.enableWallet)
	s.walletGroup.PATCH("", s.disableWallet)
	s.walletGroup.GET("", s.getWallet)
	s.walletGroup.POST("/deposits", s.deposit)
	s.walletGroup.POST("/withdrawals", s.withdraw)
}
