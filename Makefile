SHELL:=/bin/bash

migrate_up=go run main.go migrate --direction=up --step=0

test_command=richgo test ./... $(TEST_ARGS) -v --cover

migrate:
	@if [ "$(DIRECTION)" = "" ] || [ "$(STEP)" = "" ]; then\
    	$(migrate_up);\
	else\
		go run main.go migrate --direction=$(DIRECTION) --step=$(STEP);\
    fi

run-server:
	go run main.go server

internal/model/mock/mock_transaction_repository.go:
	mockgen -destination=internal/model/mock/mock_transaction_repository.go -package=mock gitlab.com/bimaahida/julo-wallet/internal/model TransactionRepository
internal/model/mock/mock_transaction_usecase.go:
	mockgen -destination=internal/model/mock/mock_transaction_usecase.go -package=mock gitlab.com/bimaahida/julo-wallet/internal/model TransactionUsecase
internal/model/mock/mock_wallet_repository.go:
	mockgen -destination=internal/model/mock/mock_wallet_repository.go -package=mock gitlab.com/bimaahida/julo-wallet/internal/model WalletRepository
internal/model/mock/mock_wallet_usecase.go:
	mockgen -destination=internal/model/mock/mock_wallet_usecase.go -package=mock gitlab.com/bimaahida/julo-wallet/internal/model WalletUsecase

mockgen: internal/model/mock/mock_transaction_repository.go\
	internal/model/mock/mock_transaction_usecase.go\
	internal/model/mock/mock_wallet_repository.go\
	internal/model/mock/mock_wallet_usecase.go

check-cognitive-complexity:
	find . -type f -name '*.go' -not -name "mock*.go" -exec gocognit -over 15 {} +


lint: check-cognitive-complexity
	golangci-lint run --print-issued-lines=false --exclude-use-default=false --enable=revive --enable=goimports  --enable=unconvert --enable=unparam --concurrency=2

test-only: check-gotest
	$(test_command)

check-gotest:
ifeq (, $(shell which richgo))
	$(warning "richgo is not installed, falling back to plain go test")
	$(eval TEST_BIN=go test)
else
	$(eval TEST_BIN=richgo test)
endif

ifdef test_run
	$(eval TEST_ARGS := -run $(test_run))
endif
	$(eval test_command=$(TEST_BIN) ./... $(TEST_ARGS) -v --cover)

test: lint test-only