-- +migrate Up notransaction
CREATE TYPE "transactions_status" AS ENUM (
  'success',
  'failed'
);

CREATE TYPE "transactions_type" AS ENUM (
  'withdraw',
  'deposit'
);

CREATE TYPE "wallets_status" AS ENUM (
  'enable',
  'disable'
);

CREATE TABLE "wallets" (
  "id" varchar PRIMARY KEY,
  "owned_by" varchar NOT NULL,
  "balance" float8 NOT NULL,
  "status" wallets_status NOT NULL,
  "enabled_at" timestamp,
  "disabled_at" timestamp,
  "created_at" timestamp NOT NULL DEFAULT (now())
);

CREATE TABLE "transactions" (
  "id" varchar PRIMARY KEY,
  "type" transactions_type NOT NULL,
  "status" transactions_status NOT NULL,
  "amount" float8 NOT NULL,
  "wallet_id" varchar NOT NULL,
  "created_by" varchar NOT NULL,
  "reference_id" varchar NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT (now())
);

CREATE INDEX "wallets_id_status_idx" ON "wallets" ("id", "status");

CREATE UNIQUE INDEX ON "wallets" ("id");

ALTER TABLE "transactions" ADD FOREIGN KEY ("wallet_id") REFERENCES "wallets" ("id");

-- +migrate Down
DROP TABLE IF EXISTS "wallets";
DROP TABLE IF EXISTS "transactions";

DROP TYPE IF EXISTS "transactions_status";
DROP TYPE IF EXISTS "transactions_type";
DROP TYPE IF EXISTS "wallets_status";
