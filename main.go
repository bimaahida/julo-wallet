// Package main :nodoc:
package main

import "gitlab.com/bimaahida/julo-wallet/internal/console"

func main() {
	console.Execute()
}
