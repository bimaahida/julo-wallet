# Mini Wallet Exercise
## Database Schema
![Database Diagram](./db/Julo.png "Database Diagram")

## Preconditions
  Before running all the scripts below we have to make sure our OS has Docker, Make, and Golang with minimum version v1.18 installed.
### Install Docker
* [How to install docker in Windows](https://docs.docker.com/desktop/install/windows-install/)
* [How to install docker in Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
* [How to install docker in macOS](https://docs.docker.com/desktop/install/mac-install/)

### Install Make
* [How to install Make in Windows](https://linuxhint.com/run-makefile-windows/)
* [How to install Make in Ubuntu](https://linuxhint.com/install-make-ubuntu/)
* [How to install Make macOS](https://formulae.brew.sh/formula/make)

### Install Golang
For installing golang I suggest using [GVM (Golang Version Manager)](https://github.com/moovweb/gvm) because it makes it easier for us to change the golang version.

## How To Run
* **Spin up docker**
    ```shell
   docker compose up -d  
   ```
* **Migrate database**
    ```shell
     make migrate
    ```
* **Run server**
    ```shell
     make run-server
    ```
## API Documentation
We can access the API with base URL http://localhost:8102/api/v1/